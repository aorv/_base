var config    = require('../config'),
    gulp      = require('gulp'),
    plugins   = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*']}),
    error     = require('../error-handler.js');

gulp.task('fonts', function () {
    return gulp.src(config.paths.fonts.src)
    .pipe(plugins.plumber({errorHandler: error.handler}))
    .pipe(gulp.dest(config.paths.fonts.dest))
});
