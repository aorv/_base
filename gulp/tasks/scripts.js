var config = require('../config'),
    gulp = require('gulp'),
    plugins = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*'] }),
    error = require('../error-handler.js');

const scriptOrder = [
    config.paths.js.src.objectInit,
    config.paths.js.src.config,
    config.paths.js.src.lib,
    config.paths.js.src.components,
    config.paths.js.src.pagesInit,
];

gulp.task('scripts', function() {
    return gulp.src([config.paths.js.src.objectInit, config.paths.js.src.config, config.paths.js.src.lib, config.paths.js.src.components, config.paths.js.src.pagesInit])
        .pipe(plugins.order(scriptOrder, { base: './' }))
        .pipe(plugins.plumber({ errorHandler: error.handler }))
        .pipe(plugins.concat('main.js'))
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(plugins.babel({ presets: ['env'] }))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(config.paths.js.dest))
        .pipe(plugins.browserSync.stream());
});

gulp.task('scripts:vendor', function() {
    return gulp.src(config.paths.js.vendorSrc)
        .pipe(plugins.plumber({ errorHandler: error.handler }))
        .pipe(plugins.concat('vendor.js'))
        .pipe(plugins.rename({ suffix: '.min' }))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(config.paths.js.vendorDest))
        .pipe(plugins.browserSync.stream());
});
