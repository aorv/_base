var config    = require('../config'),
    gulp      = require('gulp'),
    plugins   = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*']}),
    error     = require('../error-handler.js');

gulp.task('images', function () {
    return gulp.src(config.paths.img.src)
    .pipe(plugins.plumber({errorHandler: error.handler}))
    .pipe(plugins.imagemin())
    .pipe(gulp.dest(config.paths.img.dest))
    .pipe(plugins.browserSync.stream());
});
