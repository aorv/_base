var config    = require('../config'),
    gulp      = require('gulp'),
    plugins   = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*']}),
    gulpSync  = require('gulp-sync')(gulp);

gulp.task('production', gulpSync.sync(['dist-clean', 'markup:minify', 'images', 'fonts', 'styles:inline', 'styles:minify', 'scripts', 'scripts:vendor', 'revision:replace']));
