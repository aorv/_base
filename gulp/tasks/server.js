var config    = require('../config'),
    gulp      = require('gulp'),
    plugins   = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*']}),
    error     = require('../error-handler.js');

gulp.task('server', function() {
    return plugins.browserSync.init({
        proxy: config.server.proxy,
        serveStatic: ['.', config.server.basedir],
        serveStaticOptions: {
            extensions: [config.server.filetype] // pretty urls
        }
    });
});