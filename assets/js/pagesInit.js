window._base.pages = window._base.pages || {};

_base.pages = {
    pageSelector: 'data-page',
    initPage: function() {
        let page = u('body').attr(this.pageSelector);
        this[page]();
    },
    home: function() {
        console.log('Home');
    },
    contact: function() {
        console.log('Contact');
    }
};

// This is pure JS, needs jQuery document.ready
document.addEventListener('DOMContentLoaded', function() {
    u(_base.getYear.selector).text(_base.getYear.init());
    _base.pages.initPage();
}, false);
