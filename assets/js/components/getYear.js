window._base.getYear = window._base.getYear || {};

_base.getYear = {
  selector: '.js-year',
  init: function() {
    return new Date().getFullYear();
  }
};